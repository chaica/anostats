# -*- coding: utf-8 -*-
# Copyright © 2020 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get values of the main section
'''Get values of the main section'''

# standard library imports
import sys

def parse_main(cliparams, config):
    '''Parse configuration values and get values of the main section'''
    params = {}
    ##############
    # main section
    ##############
    section = 'main'
    ########################
    ### path_to_goals option
    ########################
    option = 'path_to_goals'
    if config.has_option(section, option):
        params[option] = config.get(section, option)
    ############################
    ### only_print_labels option
    ############################
    option = 'only_print_labels'
    if config.has_option(section, option):
        stringlabels = config.get(section, option)
        params[option] =  stringlabels.split(',')
    else:
        if cliparams.onlyprintlabels:
            params[option] =  cliparams.onlyprintlabels.split(',')
        else:
            params[option] =  []
    return params
