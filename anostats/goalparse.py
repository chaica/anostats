# -*- coding: utf-8 -*-
# Copyright © 2020 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get values of the configuration file
'''Get values of the configuration file'''

# standard library imports
from configparser import ConfigParser
import logging
import os
import os.path
import sys

# anostats library imports
from anostats.goalparsers.general import parse_main

def parse_goals(pathtogoals):
    '''parse the goal configurations'''
    goalconfs = []
    # extract different paths to the goal configs
    for goalname in os.listdir(pathtogoals):
        if goalname.lower().endswith('.ini'):
            goalconfs.append(os.path.join(pathtogoals, goalname))
    # iterate throug goal configs
    goalparams = {}
    goalid = 0
    for goalconf in goalconfs:
        goalid += 1
        params = {}
        # read the configuration file
        config = ConfigParser()
        if not config.read(os.path.expanduser(goalconf)):
            sys.exit('Could not read the configuration file')
        ####################
        # general section
        ####################
        params['main'] = parse_main(config)
        goalparams[goalid] = params
    return goalparams
