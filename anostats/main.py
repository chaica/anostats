# -*- coding: utf-8 -*-
# Copyright © 2020 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''Checks an RSS feed and posts new entries to Mastodon'''

# standard libraires imports
import codecs
import importlib
import logging
import logging.handlers
import sys

# app libraries imports
### parsing configurations
from anostats.cliparse import parse_cli
from anostats.confparse import parse_conf
from anostats.goalparse import parse_goals
### logs
from anostats.logs.main import parse_logs

class Main:
    '''Main class of Anonstats'''

    def __init__(self):
        self.main()

    def main(self):
        '''The main function'''
        cliparams = parse_cli()
        # parse the anostats configuration
        conf = parse_conf(cliparams)
        print(conf)
        # iterating over the different configuration files
        goalconfs = parse_goals(conf['main']['path_to_goals'])
        goalresults = parse_logs(goalconfs)
        for goal in goalconfs:
            # only print result of goal if the label in --only-print-labels
            if 'only_print_labels' in conf['main']:
                if 'label' in goalconfs[goal]['main']:
                    if goalconfs[goal]['main']['label'] in conf['main']['only_print_labels']:
                        if goalconfs[goal]['main']['type'] == 'hit_specific_page':
                            print('Goal title:{} (goal type: {}, goal id : {}) - goal result:{}'.format(goalconfs[goal]['main']['title'],
                                                                                                        goalconfs[goal]['main']['type'],
                                                                                                        goal,
                                                                                                        goalresults[goal]
                                                                                                       ))
                        if goalconfs[goal]['main']['type'] == 'hit_urls_starting_with':
                            print('Goal title:{} (goal type: {}, goal id : {}) - goal result:'.format(goalconfs[goal]['main']['title'],
                                                                                                        goalconfs[goal]['main']['type'],
                                                                                                        goal
                                                                                                       ))
                            # for url in goalresults[goal]:
                            #     print('{} : {}'.format(url, goalresults[goal][url]))
                            #gr = goalresults[goal]
                            gr = {k: v for k, v in sorted(goalresults[goal].items(), key=lambda item: item[1], reverse=True)}
                            for url in gr:
                                print('{} : {}'.format(url, gr[url]))
