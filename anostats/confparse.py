# -*- coding: utf-8 -*-
# Copyright © 2020 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get values of the configuration file
'''Get values of the configuration file'''

# standard library imports
from configparser import ConfigParser
import logging
import os
import os.path
import sys

# anostats library imports
from anostats.confparsers.general import parse_main

def parse_conf(cliparams):
    '''parse the Anostats configuration'''
    params = {}
    # read the configuration file
    config = ConfigParser()
    if not config.read(os.path.expanduser(cliparams.config)):
        sys.exit('Could not read the configuration file')
    ####################
    # feedparser section
    ####################
    params['main'] = parse_main(cliparams, config)
    return params
