# -*- coding: utf-8 -*-
# Copyright © 2020 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''Log file to import'''

# standard libraires imports
from datetime import datetime
from datetime import timedelta

class Goal:
    '''Goal for Anostats'''

    def __init__(self, conf):
        self.conf = conf
        self.main()

    def extract_log_date(self, rawdate):
        '''Transform timestamp from the log to a datetime object'''
        datestring = rawdate.strip('[').strip(']').split()[0].split(':')[0]
        dateobj = datetime.strptime(datestring, '%d/%b/%Y')
        return dateobj

    def get_period(self):
        '''extract the user-defined period and create a datetime timedelta object'''
        userperiod = self.conf['main']['period']
        # extract what period the user wants
        days = userperiod.split('_')[1]
        deltadays = timedelta(days=int(days))
        return deltadays

    def get_requested_url(self, rawrequest):
        '''extract the requested url from the raw request in the log'''
        return rawrequest.strip('"').split()[1]
