# -*- coding: utf-8 -*-
# Copyright © 2020 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''Log file to import'''

# standard libraires imports
from datetime import datetime

# app libraries imports
from anostats.goals.goal import Goal

class GoalHitSpecificPage(Goal):
    '''Goal is reached when there is a hit for a specific page'''

    def main(self):
        '''The main function'''
        pass

    def inject(self, data):
        # compare current date, the log timestamp and the period to consider
        if self.extract_log_date(data['timestamp']) > (datetime.now() - self.get_period()):
            if data['returnvalue'] == '200' and data['request'] == ' '.join(['GET', self.conf['main']['url'], 'HTTP/1.1']):
                return 1
        return 0
