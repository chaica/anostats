# -*- coding: utf-8 -*-
# Copyright © 2020 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get values of the main section
'''Get values of the main section'''

# standard library imports
import sys

def parse_main(config):
    '''Parse configuration values and get values of the main section'''
    section = 'goal'
    params = {}
    for option in ('title', 'type', 'url', 'period', 'path_to_logs', 'label'):
        if config.has_option(section, option):
            params[option] = config.get(section, option)
    return params
