# -*- coding: utf-8 -*-
# Copyright © 2020 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

# CLI parsing
'''CLI parsing'''

# standard library imports
from argparse import ArgumentParser
import logging
import os
import os.path
import sys

__version__ = '0.1'

def parse_cli():
    '''Parse CLI arguments'''
    epilog = 'For more information: https://anostats.readthedocs.io'
    description = 'Get anonymous statistics from web server logs'
    parser = ArgumentParser(prog='anostats',
                            description=description,
                            epilog=epilog)
    parser.add_argument('--version', action='version', version=__version__)
    parser.add_argument('-c', '--config',
                        default=[os.path.join(os.getenv('XDG_CONFIG_HOME', '~/.config'),
                                              'anostats.ini')],
                        dest='config',
                        help='Location of the config file (default: %(default)s)',
                        metavar='FILE')
    parser.add_argument('--only-print-labels',
                        dest='onlyprintlabels',
                        help='Only print the results for the goals with the following labels')
    params = parser.parse_args()
    print(params)
    return params
