# -*- coding: utf-8 -*-
# Copyright © 2020 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get values of the configuration file
'''Get values of the configuration file'''

# standard library imports
import os

from anostats.logs.logfactory import LogFactory

def parse_logs(goalconfs):
    '''extract data from logs'''
    # extract different paths to the goal configs
    goalresults = {}
    for goal in goalconfs:
        # initiate values for goal results
        if goalconfs[goal]['main']['type'] == 'hit_specific_page':
            goalresults[goal] = 0
        if goalconfs[goal]['main']['type'] == 'hit_urls_starting_with':
            goalresults[goal] = {}
        pathtologdir = goalconfs[goal]['main']['path_to_logs']
        # extract paths to logs
        logpaths = []
        for logname in os.listdir(pathtologdir):
            if logname.lower().endswith('.log') or logname.lower().endswith('.gz'):
                logpaths.append(os.path.join(pathtologdir, logname))
    # parse each log
    for log in logpaths:
        logf = LogFactory(log, goalconfs)
        # extract results from logs
        resultsfromlogs = logf.get_goal_results()
        for goalres in resultsfromlogs:
            # get results for goal type hit_specific_page
            if goalconfs[goalres]['main']['type'] == 'hit_specific_page':
                goalresults[goalres] += resultsfromlogs[goalres]['result']
            # get results for goal type hit_pages_starting_with
            if goalconfs[goalres]['main']['type'] == 'hit_urls_starting_with':
                for url in resultsfromlogs[goalres]['result']:
                    if url not in goalresults[goalres]:
                        goalresults[goalres][url] = 1
                    else:
                        goalresults[goalres][url] += resultsfromlogs[goalres]['result'][url]
    return goalresults
