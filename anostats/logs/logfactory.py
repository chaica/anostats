# -*- coding: utf-8 -*-
# Copyright © 2020 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''Log file to import'''

# standard libraires imports
import gzip
import re
import sys

from anostats.goals.goal import Goal
from anostats.goals.hit_specific_page import GoalHitSpecificPage
from anostats.goals.hit_urls_starting_with import GoalHitUrlsStartingWith

class LogFactory:
    '''Log treatment for Anostats'''

    def __init__(self, logpath, goalconfs):
        self.logpath = logpath
        # configurations of the goal
        self.goalconfs = goalconfs
        # will store everything about the goals
        self.goals = {}
        self.main()

    def main(self):
        '''The main function'''
        # manage plain-text log
        if self.logpath.endswith('.log') or re.fullmatch('.*\.log\.[1-9]*', self.logpath):
            with open(self.logpath) as logdesc:
                self.parse_log(logdesc)
        # manage gzipped log
        elif self.logpath.endswith('.gz'):
            with gzip.open(self.logpath, 'rt') as gzlogdesc:
                self.parse_log(gzlogdesc)

    def parse_log(self, logdesc):
        '''parse the log from its descriptor'''
        # lets create all goals
        for goal in self.goalconfs:
            if self.goalconfs[goal]['main']['type'] == 'hit_specific_page':
                self.goals[goal] = {'goalobject': GoalHitSpecificPage(self.goalconfs[goal]), 'result': 0}
            if self.goalconfs[goal]['main']['type'] == 'hit_urls_starting_with':
                self.goals[goal] = {'goalobject': GoalHitUrlsStartingWith(self.goalconfs[goal]), 'result': {}}
        # for each log lets check if the goal applies for each line
        for logline in logdesc :
            data = self.format_log(logline)
            # for each goal get the result for each log
            for goal in self.goals:
                # inject data into the goal and get the result back
                ### getting the result for the type goal hit_specific_page
                if self.goalconfs[goal]['main']['type'] == 'hit_specific_page':
                    self.goals[goal]['result'] += self.goals[goal]['goalobject'].inject(data)
                ### getting the result for the type goal hit_urls_starting_with
                if self.goalconfs[goal]['main']['type'] == 'hit_urls_starting_with':
                    result = self.goals[goal]['goalobject'].inject(data)
                    if result:
                        if result not in self.goals[goal]['result']:
                            self.goals[goal]['result'][result] = 1
                        else:
                            self.goals[goal]['result'][result] += 1

    def format_log(self, logline):
        '''transform raw line into reusable data'''
        regex = '([(\d\.)]+) - - \[(.*?)\] "(.*?)" (\d+) (\d+) "(.*?)" "(.*?)"'
        rawdata = re.match(regex, logline).groups()
        if len(rawdata) == 7:
            logdata = {'ip': rawdata[0],
                       'timestamp': rawdata[1],
                       'request': rawdata[2],
                       'returnvalue': rawdata[3],
                       'length': rawdata[4],
                       'referer': rawdata[5],
                       'user-agent': rawdata[6]
                      }
        else:
            logdata = {}
        return logdata

    def get_goal_results(self):
        '''Get results for each goal'''
        # clean goal results before returning
        for goal in self.goals:
            del self.goals[goal]['goalobject']
        return self.goals
