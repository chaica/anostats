### Anostats

Get anonymous statistics from web server logs.
For the full documentation, [read it online](https://anostats.readthedocs.io/en/latest/).

If you would like, you can [support the development of this project on Liberapay](https://liberapay.com/carlchenet/).
Alternatively you can donate cryptocurrencies:

- BTC: 1AW12Zw93rx4NzWn5evcG7RNNEM2RSLmAC
- XMR: 43GGv8KzVhxehv832FWPTF7FSVuWjuBarFd17QP163uxMaFyoqwmDf1aiRtS5jWgCiRsi73yqedNJJ6V1La2joznKHGAhDi

### Quick Install

* Install Anostats from PyPI

        # pip3 install anostats

* Install Anostats from sources
  *(see the installation guide for full details)
  [Installation Guide](http://anostats.readthedocs.io/en/latest/install.html)*


        # tar zxvf anostats-0.14.tar.gz
        # cd anostats
        # python3 setup.py install
        # # or
        # python3 setup.py install --install-scripts=/usr/bin

### Author

* Carl Chenet <chaica@ohmytux.com>

### License

This software comes under the terms of the GPLv3+.
